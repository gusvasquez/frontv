export class Estado{
  constructor(
    public id: string,
    public nombre_estado: string,
    public fecha_creacion: string,
    public fecha_actualizacion: string
  ){}
}
