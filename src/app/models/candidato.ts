export class Candidato{
  constructor(
    public id: string,
    public foto_candidato: string,
    public nombres: string,
    public apellidos: string,
    public cargo: string,
    public id_plancha: any,
    public id_estado: any
  ){}
}
