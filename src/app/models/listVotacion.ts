export class ListVotacion {
  constructor(
    public name: string,
    public value: string,
    public extra: string,
    public label: string,
    public percent: string,
    public total: string
  ){}

}
