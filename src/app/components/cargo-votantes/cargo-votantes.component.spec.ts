import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CargoVotantesComponent } from './cargo-votantes.component';

describe('CargoVotantesComponent', () => {
  let component: CargoVotantesComponent;
  let fixture: ComponentFixture<CargoVotantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CargoVotantesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CargoVotantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
