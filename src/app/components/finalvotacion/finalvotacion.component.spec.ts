import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalvotacionComponent } from './finalvotacion.component';

describe('FinalvotacionComponent', () => {
  let component: FinalvotacionComponent;
  let fixture: ComponentFixture<FinalvotacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalvotacionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FinalvotacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
