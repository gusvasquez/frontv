import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroTarjetonesComponent } from './registro-tarjetones.component';

describe('RegistroTarjetonesComponent', () => {
  let component: RegistroTarjetonesComponent;
  let fixture: ComponentFixture<RegistroTarjetonesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroTarjetonesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistroTarjetonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
