import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroVotantesComponent } from './registro-votantes.component';

describe('RegistroVotantesComponent', () => {
  let component: RegistroVotantesComponent;
  let fixture: ComponentFixture<RegistroVotantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroVotantesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistroVotantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
